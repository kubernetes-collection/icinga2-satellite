#!/bin/bash


export satellite=$(kubectl get pod -n icinga2 | tail -n 1 | awk '{ print $1 }')

follow=$2


function get_pod {
        kubectl get pod -n icinga2 $satellite
}


function describe_pod {
        kubectl describe pod -n icinga2 $satellite
}


function logs_pod {
        if [[ $follow == "true" ]] ; then
                kubectl logs -n icinga2 -f $satellite
        else
                kubectl logs -n icinga2 $satellite
        fi
}


function exec_pod {
        kubectl exec -ti -n icinga2 $satellite -- /bin/bash
}


case $1 in
        get)
           get_pod
        ;;
        describe)
           describe_pod
        ;;
        logs)
           logs_pod
        ;;
        exec)
           exec_pod
        ;;
        *)
           echo "Use get|describe|logs|exec"
        ;;
esac
