#!/usr/bin/python3
#
# Author        Lukas Rueckerl
# mailto        lukas.rueckerl@edv-bv.de
#
version = "0/15072022"
#
# Tested with Python 3.10.2
#


################################################################################
# Import and preset variables

from email import message
from email.errors import MessageDefect
import re
from pysnmp import hlapi
import argparse
import sys

# Possible Exit codes
OK = 0
WARNING = 1
CRITICAL = 2
DOWN = 3

################################################################################
# Define shell arguments

p = argparse.ArgumentParser (epilog='nutanix2icinga | Version '+version+' | ©edv-bv GmbH',)

sp = p.add_subparsers (dest = 'check', title = "checks")

# Check for alerts
get_p = sp.add_parser ('sophosdisk', help = 'Gets an OID value')
get_p = sp.add_parser ('memoryusage', help = 'Gets an OID value')
get_p = sp.add_parser ('sophoscpu1', help = 'Gets an OID value')
get_p = sp.add_parser ('sophoscpu2', help = 'Gets an OID value')
get_p = sp.add_parser ('sophoscpu3', help = 'Gets an OID value')
get_p = sp.add_parser ('sophoscpu4', help = 'Gets an OID value')
get_p = sp.add_parser ('hacurrentstatus', help = 'Gets an OID value')
get_p = sp.add_parser ('hapeerstatus', help = 'Gets an OID value')
get_p = sp.add_parser ('sophosinterfacestatus', help = 'Gets an OID value')
get_p = sp.add_parser ('sophosinterfacediscards', help = 'Gets an OID value')
get_p = sp.add_parser ('sophosinterfaceerrors', help = 'Gets an OID value')


# Required Arguments
p.add_argument ('-host', help = "FQDN or IP of the system")
p.add_argument ('-user', help = "SNMPv3 User")
p.add_argument ('-privkey', help = "SNMPv3 Priv-Password")
p.add_argument ('-authkey', help = "SNMPv3 Auth-Password")
p.add_argument ('-privprot', help = "SNMPv3 Priv-Protocol")
p.add_argument ('-authprot', help = "SNMPv3 Auth-Protocol")

################################################################################

# Define Functions

def construct_object_types(list_of_oids):
    object_types = []
    for oid in list_of_oids:
        object_types.append(hlapi.ObjectType(hlapi.ObjectIdentity(oid)))
    return object_types


def construct_value_pairs(list_of_pairs):
    pairs = []
    for key, value in list_of_pairs.items():
        pairs.append(hlapi.ObjectType(hlapi.ObjectIdentity(key), value))
    return pairs


def get(target, oids, credentials, port=161, engine=hlapi.SnmpEngine(), context=hlapi.ContextData()):
    handler = hlapi.getCmd(
        engine,
        credentials,
        hlapi.UdpTransportTarget((target, port)),
        context,
        *construct_object_types(oids)
    )
    return fetch(handler, 1)[0]


def set(target, value_pairs, credentials, port=161, engine=hlapi.SnmpEngine(), context=hlapi.ContextData()):
    handler = hlapi.setCmd(
        engine,
        credentials,
        hlapi.UdpTransportTarget((target, port)),
        context,
        *construct_value_pairs(value_pairs)
    )
    return fetch(handler, 1)[0]


def get_bulk(target, oids, credentials, count, start_from=0, port=161,
             engine=hlapi.SnmpEngine(), context=hlapi.ContextData()):
    handler = hlapi.bulkCmd(
        engine,
        credentials,
        hlapi.UdpTransportTarget((target, port)),
        context,
        start_from, count,
        *construct_object_types(oids)
    )
    return fetch(handler, count)


def get_bulk_auto(target, oids, credentials, count_oid, start_from=0, port=161,
                  engine=hlapi.SnmpEngine(), context=hlapi.ContextData()):
    count = get(target, [count_oid], credentials, port, engine, context)[count_oid]
    return get_bulk(target, oids, credentials, count, start_from, port, engine, context)


def cast(value):
    try:
        return int(value)
    except (ValueError, TypeError):
        try:
            return float(value)
        except (ValueError, TypeError):
            try:
                return str(value)
            except (ValueError, TypeError):
                pass
    return value


def fetch(handler, count):
    result = []
    for i in range(count):
        try:
            error_indication, error_status, error_index, var_binds = next(handler)
            if not error_indication and not error_status:
                items = {}
                for var_bind in var_binds:
                    items[str(var_bind[0])] = cast(var_bind[1])
                result.append(items)
            else:
                raise RuntimeError('Got SNMP error: {0}'.format(error_indication))
        except StopIteration:
            break
    return result

def get_sophosdisk(host, oid, user, authkey, privkey, authprot, privprot):
        ret_code = OK
        msg = ""
        perf_data = []
        value = get(host, oid, hlapi.UsmUserData(user,authKey=authkey, privKey=privkey,authProtocol=authprot, privProtocol=privprot))[oid[0]]
        msg = str(value)
        if int(value) > 80:
                ret_code = WARNING
        if int(value) > 90:
                ret_code = CRITICAL
        msg="Total Disk Usage: "+str(value)+"%"
        return (ret_code, msg, perf_data)

def get_memoryusage(host, oid, user, authkey, privkey, authprot, privprot):
        ret_code = OK
        msg = ""
        perf_data = []
        value = get(host, oid, hlapi.UsmUserData(user,authKey=authkey, privKey=privkey,authProtocol=authprot, privProtocol=privprot))[oid[0]]
        msg = str(value)
        if int(value) > 80:
                ret_code = WARNING
        if int(value) > 90:
                ret_code = CRITICAL
        msg="Total Memory Usage: "+str(value)+"%"
        return (ret_code, msg, perf_data)

def get_sophoscpu1(host, oid, user, authkey, privkey, authprot, privprot):
        ret_code = OK
        msg = ""
        perf_data = []
        value = get(host, oid, hlapi.UsmUserData(user,authKey=authkey, privKey=privkey,authProtocol=authprot, privProtocol=privprot))[oid[0]]
        msg = str(value)
        if int(value) > 80:
               ret_code = WARNING
        if int(value) > 90:
               ret_code = CRITICAL
        msg="Total CPU Usage: "+str(value)+"%"
        return (ret_code, msg, perf_data)

def get_sophoscpu2(host, oid, user, authkey, privkey, authprot, privprot):
        ret_code = OK
        msg = ""
        perf_data = []
        value = get(host, oid, hlapi.UsmUserData(user,authKey=authkey, privKey=privkey,authProtocol=authprot, privProtocol=privprot))[oid[0]]
        msg = str(value)
        if int(value) > 80:
               ret_code = WARNING
        if int(value) > 90:
               ret_code = CRITICAL
        msg="Total CPU Usage: "+str(value)+"%"
        return (ret_code, msg, perf_data)
        
def get_sophoscpu3(host, oid, user, authkey, privkey, authprot, privprot):
        ret_code = OK
        msg = ""
        perf_data = []
        value = get(host, oid, hlapi.UsmUserData(user,authKey=authkey, privKey=privkey,authProtocol=authprot, privProtocol=privprot))[oid[0]]
        msg = str(value)
        if int(value) > 80:
               ret_code = WARNING
        if int(value) > 90:
               ret_code = CRITICAL
        msg="Total CPU Usage: "+str(value)+"%"
        return (ret_code, msg, perf_data)
        
def get_sophoscpu4(host, oid, user, authkey, privkey, authprot, privprot):
        ret_code = OK
        msg = ""
        perf_data = []
        value = get(host, oid, hlapi.UsmUserData(user,authKey=authkey, privKey=privkey,authProtocol=authprot, privProtocol=privprot))[oid[0]]
        msg = str(value)
        if int(value) > 80:
               ret_code = WARNING
        if int(value) > 90:
               ret_code = CRITICAL
        msg="Total CPU Usage: "+str(value)+"%"
        return (ret_code, msg, perf_data)

def get_sophosinterfacestatus(host, oid, user, authkey, privkey, authprot, privprot):
        ret_code = OK
        msg = ""
        perf_data = []
        value = get(host, oid, hlapi.UsmUserData(user,authKey=authkey, privKey=privkey,authProtocol=authprot, privProtocol=privprot))[oid[0]]
        msg = str(value)
        if value==2:
                ret_code = CRITICAL
        msg="Interface Status: DOWN / Admin STATUS: UP"
        if value==3:
                ret_code = WARNING
        msg="Interface Status: in Testing Mode"
        if value==4:
                ret_code = CRITICAL
        msg="Interface Status: Unknown"
        if value==5:
                ret_code = CRITICAL
        msg="Interface Status: dormant"
        if value==6:
                ret_code = CRITICAL
        msg="Interface Status: not present"
        if value==7:
                ret_code = CRITICAL
        msg="Interface Status: Lower Layer Down"
        return (ret_code, msg, perf_data)

def get_sophosinterfacediscards(host, oid, user, authkey, privkey, authprot, privprot):
        ret_code = OK
        msg = ""
        perf_data = []
        value = get(host, oid, hlapi.UsmUserData(user,authKey=authkey, privKey=privkey,authProtocol=authprot, privProtocol=privprot))[oid[0]]
        msg = str(value)
        if int(value) != 0:
                ret_code = CRITICAL
        msg="Total CPU Usage: "+str(value)+"%"
        return (ret_code, msg, perf_data)

def get_sophosinterfaceerrors(host, oid, user, authkey, privkey, authprot, privprot):
        ret_code = OK
        msg = ""
        perf_data = []
        value = get(host, oid, hlapi.UsmUserData(user,authKey=authkey, privKey=privkey,authProtocol=authprot, privProtocol=privprot))[oid[0]]
        msg = str(value)
        if int(value) != 0:
                ret_code = CRITICAL
        msg="Total CPU Usage: "+str(value)+"%"
        return (ret_code, msg, perf_data)


def get_hacurrentstatus(host, oid, user, authkey, privkey, authprot, privprot):
        ret_code = OK
        msg = ""
        perf_data = []
        value = get(host, oid, hlapi.UsmUserData(user,authKey=authkey, privKey=privkey,authProtocol=authprot, privProtocol=privprot))[oid[0]]
        msg = str(value)
        if int(value)==0:
                ret_code = CRITICAL
        msg="Current HA Status: NOT APPLICABLE"
        if int(value)==1:
                ret_code = CRITICAL
        msg="Current HA Status: AUXILIARY"
        if int(value)==2:
                ret_code = CRITICAL
        msg="Current HA Status: STAND ALONE"
        if int(value)==3:
                ret_code = CRITICAL
        msg="Current HA Status: PRIMARY"
        if int(value)==4:
                ret_code = CRITICAL
        msg="Current HA Status: FAULTY"
        return (ret_code, msg, perf_data)

def get_hapeerstatus(host, oid, user, authkey, privkey, authprot, privprot):
        ret_code = OK
        msg = ""
        perf_data = []
        value = get(host, oid, hlapi.UsmUserData(user,authKey=authkey, privKey=privkey,authProtocol=authprot, privProtocol=privprot))[oid[0]]
        msg = str(value)
        if int(value)==0:
                ret_code = CRITICAL
        msg="Current HA Status: NOT APPLICABLE"
        if int(value)==1:
                ret_code = CRITICAL
        msg="Current HA Status: AUXILIARY"
        if int(value)==2:
                ret_code = CRITICAL
        msg="Current HA Status: STAND ALONE"
        if int(value)==3:
                ret_code = CRITICAL
        msg="Current HA Status: PRIMARY"
        if int(value)==4:
                ret_code = CRITICAL
        msg="Current HA Status: FAULTY"
        return (ret_code, msg, perf_data)

checks = {
        'sophosdisk' : {
                'oid' : ["1.3.6.1.4.1.2604.5.1.2.4.2.0"],
                'function' : get_sophosdisk
        },
        'memoryusage' : {
                'oid' : ["1.3.6.1.4.1.2604.5.1.2.5.2.0"],
                'function' : get_memoryusage
        },
        'sophoscpu1' : {
                'oid' : ["1.3.6.1.2.1.25.3.3.1.2.196608"],
                'function' : get_sophoscpu1
        },
        'sophoscpu2' : {
                'oid' : ["1.3.6.1.2.1.25.3.3.1.2.196609"],
                'function' : get_sophoscpu1
        },
        'sophoscpu3' : {
                'oid' : ["1.3.6.1.2.1.25.3.3.1.2.196610"],
                'function' : get_sophoscpu1
        },
        'sophoscpu4' : {
                'oid' : ["1.3.6.1.2.1.25.3.3.1.2.196611"],
                'function' : get_sophoscpu1
        },
        'hacurrentstatus' : {
                'oid' : ["1.3.6.1.4.1.2604.5.1.4.4.0"],
                'function' : get_hacurrentstatus
    },
        'hapeerstatus' : {
                'oid' : ["1.3.6.1.4.1.2604.5.1.4.5.0"],
                'function' : get_hapeerstatus
    },
        'sophosinterfacestatus' : {
                'oid' : ["1.3.6.1.2.1.2.2.1.8"],
                'function' : get_sophosinterfacestatus
        },
        'sophosinterfacediscards' : {
                'oid' : ["1.3.6.1.2.1.2.2.1.13"],
                'function' : get_sophosinterfacediscards
        },
        'sophosinterfaceerrors' : {
                'oid' : ["1.3.6.1.2.1.2.2.1.14"],
                'function' : get_sophosinterfaceerrors
        },
}

authprot = {
        'MD5' : hlapi.usmHMACMD5AuthProtocol,
        'SHA' : hlapi.usmHMACSHAAuthProtocol,
        'SHA-256' : hlapi.usmHMAC256SHA384AuthProtocol,
        'SHA-512' : hlapi.usmHMAC384SHA512AuthProtocol

}

privprot = {
        'DES' : hlapi.usmDESPrivProtocol,
        'AES' : hlapi.usmAesCfb128Protocol,
        'AES-192' : hlapi.usmAesCfb192Protocol,
        'AES-256' : hlapi.usmAesCfb256Protocol

}

################################################################################
# Import input parameters and execute function that was called

args = p.parse_args ()

(ret_code, msg, perf_data) = checks[args.check]['function'] (args.host, checks[args.check]['oid'] , args.user, args.authkey , args.privkey, authprot[args.authprot], privprot[args.privprot])

################################################################################
# Output results

if ret_code == 1:
        msg = "WARNING: " + msg
elif ret_code == 2:
        msg = "CRITICAL: " + msg

if perf_data:
        msg += "| " + " ".join (sorted (perf_data))

print (msg.rstrip ())
sys.exit (ret_code)
