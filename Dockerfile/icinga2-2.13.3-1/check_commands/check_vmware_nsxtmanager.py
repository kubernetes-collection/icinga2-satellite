#!/usr/bin/python3
#
SCRIPTNAME= "nsxtmanager2icinga"
# Author	Lukas Rueckerl 
# mailto 	info@lkrkl.dev 
VERSION=	"0.1 / 200423"
#
# Tested with Python 3.11
#
# Aggregates metrics from the NSX-T Manager and Policy API for monitoring - intended for Icinga2
# Be sure to install the "requests"-Pip-Package beforehand


################################################################################
# Import and preset variables 

import argparse
import json
import sys
import decimal
from tokenize import String
import requests
import urllib.parse
from requests.auth import HTTPBasicAuth
from requests.packages.urllib3.exceptions import InsecureRequestWarning # type: ignore
requests.packages.urllib3.disable_warnings(InsecureRequestWarning) # type: ignore

################################################################################
# Define global Variables
OK = 0
WARNING = 1
CRITICAL = 2
DOWN = 3

################################################################################
# Define shell arguments

p = argparse.ArgumentParser (epilog=SCRIPTNAME+' | '+VERSION+' | CC-BY',)

sp = p.add_subparsers (dest = 'check', title = "checks")
clusterstatus_p = sp.add_parser ('clusterstatus', help = 'Returns your current cluster status and will exit with Critical Status if not in a stable state')
alarms_p = sp.add_parser ('alarms', help = 'Returns open Alarms. The severity level of the alarms determines the exit code.')
managerconnectivity_p = sp.add_parser ('managerconnectivity', help = 'Aggregates connection details between the management clusters nodes interlink and watches for Round-Trip-Time and Packet Loss')
nodestatus_p = sp.add_parser ('nodestatus', help = 'Checks all nodes in the cluster for resource and control plane availability')

# Required Arguments
p.add_argument ('-host', help = "FQDN or IP of your NSX-T Manager VIP")
p.add_argument ('-username', help = "API-User")
p.add_argument ('-password', help = "API-User's Password")
p.add_argument ('-node', help = "The specific resource to query. Provide the displayed name for the Manager Instance from the manager UI.", required=False)

################################################################################
# Check functions

def call_api_get (host,uri, username, password, returnToken = False):
	url = "https://"+host+uri
	req = requests.get(url, auth=HTTPBasicAuth(username, password), verify = False)

	if req.status_code != 200:
		print ("UNKNOWN ERROR: Can't connect to %s failed: %s" % (url, "error"))
		sys.exit (DOWN)

	result = req.json()

	return result

def call_api_post (host,uri, username, password, payload):	
	url = "https://"+host+uri
	headers = {}

	req = requests.post (url, data = payload, headers = headers, auth = HTTPBasicAuth(username, password), verify = False)

	if req.status_code != 200:
		print ("UNKNOWN ERROR: Can't connect to %s failed: %s" % (url, "error"))
		sys.exit (DOWN)

	result = req.json()

	return result

def get_clusterStatus (host, uri, username, password, node = False, critical = False, warning = False):
	ret_code = OK
	msg = "OK"
	perf_data = []

	response = call_api_get(host,uri,username,password)

	if (response["mgmt_cluster_status"]["status"]) != "STABLE":
		ret_code = CRITICAL	
		msg = "CRITICAL - Cluster Status is "+str(response["mgmt_cluster_status"]["status"])

	if ret_code == OK:
		msg = "OK"

	return (ret_code, msg, perf_data)

def get_clusterAlarms (host, uri, username, password, node = False, critical = False, warning = False):
	ret_code = OK
	msg = "OK"
	perf_data = []

	response = call_api_get(host,uri,username,password)

	if response["result_count"] != 0:
		ret_code = WARNING	
		msg = ""
	
	for alarmEvent in response["results"]:
		if alarmEvent["severity"] == "MEDIUM" or alarmEvent["severity"] == "LOW":
			msg = msg+"["+alarmEvent["node_display_name"]+"] "+alarmEvent["description"]+" ("+alarmEvent["severity"]+")\n"

		if alarmEvent["severity"] == "CRTIICAL" or alarmEvent["severity"] == "HIGH":
			ret_code = CRITICAL
			msg = alarmEvent["node_display_name"]+" reports: "+alarmEvent["description"]+" ("+alarmEvent["severity"]+")\n"+msg

	if ret_code == OK:
		msg = "OK"

	return (ret_code, msg, perf_data)

def get_managerconnectivity (host, uri, username, password, node = False, critical = False, warning = False):
	ret_code = OK
	msg = ""
	perf_data = []

	response = call_api_get(host,uri,username,password)

	total_connections=0
	avg_rtt_sum=0
	avg_loss_sum=0
	total_connections=0

	for endpoint in response["results"]:
		for connection in endpoint["latency_data"]:
			total_connections+=1
			avg_rtt_sum+=int(connection["rtt_avg"])
			avg_loss_sum+=int(connection["packet_loss_percent"].strip('%'))

			if str(connection["packet_loss_percent"]) != "0%":
				ret_code = WARNING
				msg += "Connection ["+endpoint["name"]+"]-["+connection["destination_node_name"]+"] has Packet Loss "+str(connection["packet_loss_percent"])
			if str(connection["status"]) != "up":
				ret_code = CRITICAL
				msg += "Connection ["+endpoint["name"]+"]-["+connection["destination_node_name"]+"] has Status "+str(connection["status"])

	avg_rtt=avg_rtt_sum/total_connections
	avg_loss=avg_loss_sum/total_connections

	if ret_code == OK:
		msg = "OK, Total Avg. RTT: "+str(avg_rtt)+"ms, Total Avg. Loss: "+str(avg_loss)

	return (ret_code, msg, perf_data)

def get_nodestatus (host, uri, username, password, node = False, critical = False, warning = False):
	ret_code = OK
	msg = ""
	perf_data = []

	if node == False:
		print("Provide a display name for the node you want to check")
		exit()

	uri += str(get_nodeId(host, username, password, node))+"/status"
	
	response = call_api_get(host,uri,username,password)

	totaldiskusage = (response["system_status"]["disk_space_used"]/response["system_status"]["disk_space_total"])*100
	if totaldiskusage >= 80 and totaldiskusage < 90:
		if ret_code != CRITICAL: 
			ret_code = WARNING
		msg += "Total disk usage: "+str(totaldiskusage)		
	if totaldiskusage >= 90:
		ret_code = CRITICAL
		msg += "Total disk usage: "+str(totaldiskusage)		

	for filesystem in response["system_status"]["file_systems"]:
		diskusage = (filesystem["used"]/filesystem["total"])*100
		if diskusage >= 80:
			if ret_code != CRITICAL: 
				ret_code = WARNING
		if diskusage >= 90:
			ret_code = CRITICAL			

	if ret_code == OK:
		msg = "OK, Total Disk Usage: "+str(round(totaldiskusage))+"%"

	return (ret_code, msg, perf_data)

def get_nodeId(host, username, password, query):
	nodes = call_api_get(host,"/api/v1/cluster/nodes/",username,password)

	results = []

	for node in nodes["results"]:
		if node["display_name"] == query:
			results += [node]	

	if len(results) != 0 and len(results) <2:
		return results[0]["id"]
	else:
		print("Cant find Paramter Node. Please specify.")
		exit()





################################################################################
# API Endpoint definition

checks = {
	'clusterstatus' : {
		'api_path' : "/api/v1/cluster/status",
		'function' : get_clusterStatus
	},
	'alarms' : {
		'api_path' : "/api/v1/alarms?status=OPEN",
		'function' : get_clusterAlarms
	},
	'managerconnectivity' : {
		'api_path' : "/api/v1/systemhealth/appliances/latency/status",
		'function' : get_managerconnectivity
	},
	'nodestatus' : {
		'api_path' : "/api/v1/cluster/nodes/",
		'function' : get_nodestatus
	}
}

################################################################################
# Import input parameters and execute function that was called

args = p.parse_args ()

(ret_code, msg, perf_data) = checks[args.check]['function'] (str(args.host), checks[args.check]['api_path'], args.username, args.password, args.node)

################################################################################
# Output results

if ret_code == 1:
	msg = "WARNING: " + msg
elif ret_code == 2:
	msg = "CRITICAL: " + msg

if perf_data:
	msg += "| " + " ".join (sorted (perf_data))

print (msg.rstrip ())
sys.exit (ret_code)

