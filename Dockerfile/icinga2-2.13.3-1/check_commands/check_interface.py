#!/usr/bin/python3
#
# Author        Lukas Rueckerl
# mailto        lukas.rueckerl@edv-bv.de
#
version = "0/15072022"
#
# Tested with Python 3.10.2
#


################################################################################
# Import and preset variables

from email import message
from email.errors import MessageDefect
import re
from pysnmp import hlapi
import argparse
import sys
import json

# Possible Exit codes
OK = 0
WARNING = 1
CRITICAL = 2
DOWN = 3

################################################################################
# Define shell arguments

p = argparse.ArgumentParser (epilog='interfaces2icinga | Version '+version+' | ©edv-bv GmbH',)

sp = p.add_subparsers (dest = 'check', title = "checks")

# Check for alerts
get_p = sp.add_parser ('interfacestatus', help = 'Gets an OID value')
get_p = sp.add_parser ('interfacediscards', help = 'Gets an OID value')
get_p = sp.add_parser ('interfaceerrors', help = 'Gets an OID value')
get_p = sp.add_parser ('interfacelist', help = 'Prints a list of all interfaces')

# Required Arguments
p.add_argument ('-host', help = "FQDN or IP of the system", required=True)
p.add_argument ('-version', help = "Set SNMP version to 2c or 3", required=True)
p.add_argument ('-community', help = "Set SNMP 2c Community",default=None)
p.add_argument ('-user', help = "SNMPv3 User",default=None)
p.add_argument ('-privkey', help = "SNMPv3 Priv-Password",default=None)
p.add_argument ('-authkey', help = "SNMPv3 Auth-Password",default=None)
p.add_argument ('-privprot', help = "SNMPv3 Priv-Protocol",default=None)
p.add_argument ('-authprot', help = "SNMPv3 Auth-Protocol",default=None)
p.add_argument ('-name', help = "Displayname of the interface", required=True)

################################################################################

# Define Functions

def construct_object_types(list_of_oids):
    object_types = []
    for oid in list_of_oids:
        object_types.append(hlapi.ObjectType(hlapi.ObjectIdentity(oid)))
    return object_types


def construct_value_pairs(list_of_pairs):
    pairs = []
    for key, value in list_of_pairs.items():
        pairs.append(hlapi.ObjectType(hlapi.ObjectIdentity(key), value))
    return pairs

def authenticate(version, community=None, user=None, privkey=None, authkey=None, privprot=None, authprot=None):
    if version == "3":
        return hlapi.UsmUserData(user,authKey=authkey, privKey=privkey,authProtocol=authprot, privProtocol=privprot)
    if version == "2c":
        if community == None:
            return -2
        return hlapi.CommunityData(community,mpModel=1)
    return -1


def get(target, oids, credentials, port=161, engine=hlapi.SnmpEngine(), context=hlapi.ContextData()):
    handler = hlapi.getCmd(
        engine,
        credentials,
        hlapi.UdpTransportTarget((target, port)),
        context,
        *construct_object_types(oids)
    )
    return fetch(handler, 1)[0]


def set(target, value_pairs, credentials, port=161, engine=hlapi.SnmpEngine(), context=hlapi.ContextData()):
    handler = hlapi.setCmd(
        engine,
        credentials,
        hlapi.UdpTransportTarget((target, port)),
        context,
        *construct_value_pairs(value_pairs)
    )
    return fetch(handler, 1)[0]


def get_bulk(target, oids, credentials, count, start_from=0, port=161,
             engine=hlapi.SnmpEngine(), context=hlapi.ContextData()):
    handler = hlapi.bulkCmd(
        engine,
        credentials,
        hlapi.UdpTransportTarget((target, port)),
        context,
        start_from, count,
        *construct_object_types(oids),
        lexicographicMode = False
    )
    return fetch(handler, count)


def get_bulk_auto(target, oids, credentials, count_oid, start_from=0, port=161,
                  engine=hlapi.SnmpEngine(), context=hlapi.ContextData()):
    count = get(target, [count_oid], credentials, port, engine, context)[count_oid]
    return get_bulk(target, oids, credentials, count, start_from, port, engine, context)


def cast(value):
    try:
        return int(value)
    except (ValueError, TypeError):
        try:
            return float(value)
        except (ValueError, TypeError):
            try:
                return str(value)
            except (ValueError, TypeError):
                pass
    return value


def fetch(handler, count):
    result = []
    for i in range(count):
        try:
            error_indication, error_status, error_index, var_binds = next(handler)
            if not error_indication and not error_status:
                items = {}
                for var_bind in var_binds:
                    items[str(var_bind[0])] = cast(var_bind[1])
                result.append(items)
            else:
                raise RuntimeError('Got SNMP error: {0}'.format(error_indication)+" "+format(error_status))            
                
        except StopIteration:
            break
    return result


def get_interface_oid (host, oid, authorization, name):
        ret_code = OK
        msg = ""
        perf_data = []
        value = (get_bulk(host, oid, authorization, 128, start_from = 1))
        interfaces = []
        for key in value:
            oid = list(key)[0]
            if key[oid] != '':
                interfaces.append ([oid,key[oid]])
        for interface in interfaces:
            if interface[1] == name:
                return (interface[0])
                break
        return -1

def get_interfacelist (host, oid, authorization, name = None):
        ret_code = OK
        msg = ""
        perf_data = []
        value = (get_bulk(host, oid, authorization, 128, start_from = 1))
        interfaces = []
        for key in value:
            oid = list(key)[0]
            if key[oid] != '':
                interfaces.append ([oid,key[oid]])
        for interface in interfaces:
            msg += interface[1]+", "
        return (ret_code, msg, perf_data) 

def get_interfacestatus(host, oid, authorization, name):
        ret_code = OK
        msg = "Interface Status "+str(name)+": UP / Admin STATUS: UP  ok"
        perf_data = []
        ifindex = get_interface_oid(host, ["1.3.6.1.2.1.31.1.1.1.1"], authorization, name)
        ifindex = ifindex.split('.')[-1]  # type: ignore
        oid[0] = oid[0]+"."+str(ifindex)
        value = get(host, oid, authorization)[oid[0]]
        if value==2:
                ret_code = CRITICAL
                msg="Interface Status: DOWN / Admin STATUS: UP"
        if value==3:
                ret_code = WARNING
                msg="Interface Status: in Testing Mode"
        if value==4:
                ret_code = CRITICAL
                msg="Interface Status: Unknown"
        if value==5:
                ret_code = CRITICAL
                msg="Interface Status: dormant"
        if value==6:
                ret_code = CRITICAL
                msg="Interface Status: not present"
        if value==7:
                ret_code = CRITICAL
                msg="Interface Status: Lower Layer Down"
        return (ret_code, msg, perf_data)

def get_interfacediscards(host, oid, authorization, name):
        ret_code = OK
        msg = "interfacediscards: 0"
        perf_data = []
        ifindex = get_interface_oid(host, ["1.3.6.1.2.1.31.1.1.1.1"], authorization, name)
        ifindex = ifindex.split ('.')[-1]  # type: ignore
        oid[0] = oid[0]+"."+str(ifindex)
        value = get(host, oid, authorization)[oid[0]]
        if int(value) != 0:
                ret_code = CRITICAL
                msg="Interfacediscards: "+str(value)
        return (ret_code, msg, perf_data)

def get_interfaceerrors(host, oid, authorization, name):
        ret_code = OK
        msg = "interfaceerrors: 0"
        perf_data = []
        ifindex = get_interface_oid(host, ["1.3.6.1.2.1.31.1.1.1.1"], authorization, name)
        ifindex = ifindex.split ('.')[-1]  # type: ignore  
        oid[0] = oid[0]+"."+str(ifindex)
        value = get(host, oid, authorization)[oid[0]]
        if int(value) != 0:
                ret_code = CRITICAL
                msg="Interfaceerrors: "+str(value)
        return (ret_code, msg, perf_data)

checks = {
        'interfacestatus' : {
                'oid' : ["1.3.6.1.2.1.2.2.1.8"],
                'function' : get_interfacestatus
        },
        'interfacediscards' : {
                'oid' : ["1.3.6.1.2.1.2.2.1.13"],
                'function' : get_interfacediscards
        },
        'interfaceerrors' : {
                'oid' : ["1.3.6.1.2.1.2.2.1.14"],
                'function' : get_interfaceerrors
        },
        'interfacelist' : {
                'oid' : ["1.3.6.1.2.1.31.1.1.1.1"],
                'function' : get_interfacelist
        }
}

authprot_lookup = {
        'MD5' : hlapi.usmHMACMD5AuthProtocol,
        'SHA' : hlapi.usmHMACSHAAuthProtocol,
        'SHA-256' : hlapi.usmHMAC256SHA384AuthProtocol,
        'SHA-512' : hlapi.usmHMAC384SHA512AuthProtocol

}

privprot_lookup = {
        'DES' : hlapi.usmDESPrivProtocol,
        'AES' : hlapi.usmAesCfb128Protocol,
        'AES-192' : hlapi.usmAesCfb192Protocol,
        'AES-256' : hlapi.usmAesCfb256Protocol

}

################################################################################

# Import input parameters
args = p.parse_args ()

# Create authorization Object and check if valid
authorization = authenticate(args.version, args.community, args.user, args.privkey, args.authkey, privprot=privprot_lookup[args.privprot], authprot=authprot_lookup[args.authprot])
if authorization == -1:
    print('No valid SNMP Version')
    sys.exit (CRITICAL)
if authorization == -2:
    print('No Credentials specified')
    sys.exit (CRITICAL)

# Execute check function and save returning data
(ret_code, msg, perf_data) = checks[args.check]['function'] (args.host, checks[args.check]['oid'] , authorization, args.name)

################################################################################

# Output results
if ret_code == 1:
        msg = "WARNING: " + msg
elif ret_code == 2:
        msg = "CRITICAL: " + msg

if perf_data:
        msg += "| " + " ".join (sorted (perf_data))

print (msg.rstrip ())

# Exit with Code
sys.exit (ret_code)
