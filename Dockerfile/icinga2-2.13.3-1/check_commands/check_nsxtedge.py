#!/usr/bin/python3
#
SCRIPTNAME= "nsxtedge2icinga"
# Author	Lukas Rueckerl 
# mailto 	info@lkrkl.dev 
VERSION=	"0.1 / 200423"
#
# Tested with Python 3.11
#
# Aggregates metrics from the NSX-T Manager and Policy API for monitoring - intended for Icinga2
# Be sure to install the "requests"-Pip-Package beforehand


################################################################################
# Import and preset variables 

import argparse
import json
import sys
import decimal
from tokenize import String
import requests
import urllib.parse
from requests.auth import HTTPBasicAuth
from requests.packages.urllib3.exceptions import InsecureRequestWarning
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

################################################################################
# Define global Variables
OK = 0
WARNING = 1
CRITICAL = 2
DOWN = 3

################################################################################
# Define shell arguments

p = argparse.ArgumentParser (epilog=SCRIPTNAME+' | '+VERSION+' | CC-BY Lukas Rückerl, info@lkrkl.dev',)

sp = p.add_subparsers (dest = 'check', title = "checks")
edgeclusterstatus = sp.add_parser ('edgeclusterstatus', help = 'Returns your current Edge cluster status and will exit with Critical Status if not in a stable state')
edgenodestatus = sp.add_parser ('edgenodestatus', help = 'Returns current overall state of the edge node')
edgenodetunnelstatus = sp.add_parser ('edgenodetunnelstatus', help = 'Checks the Tunnel status between Edge and Transport Nodes of your overlay network.')
edgenodeconnectionstatus = sp.add_parser ('edgenodeconnectionstatus', help = 'Checks the status of the management and control plane connections')
edgenoderesourcestatus = sp.add_parser ('edgenoderesourcestatus', help = 'Returns Edgenode Resources (CPU load, Memory and Disk Usage. Will result in WARNING if any of these is over 80 percent used and CRITICAL if over 90 percent)')

# Required Arguments
p.add_argument ('-host', help = "FQDN or IP of your NSX-T Manager VIP")
p.add_argument ('-username', help = "API-User")
p.add_argument ('-password', help = "API-User's Password")
p.add_argument ('-edgecluster', help = "The specific resource to query. Provide the displayed name from the manager UI. Can leave blank if only one cluster is present.", required=False)
p.add_argument ('-edgenode', help = "The specific resource to query. Provide the displayed name from the manager UI.", required=False)

################################################################################
# Check functions

def call_api_get (host,uri, username, password, returnToken = False):
	url = "https://"+host+uri
	req = requests.get(url, auth=HTTPBasicAuth(username, password), verify = False)

	if req.status_code != 200:
		print ("UNKNOWN ERROR: Can't connect to %s failed: %s" % (url, "error"))
		sys.exit (DOWN)

	result = req.json()

	return result

def call_api_post (host,uri, username, password, payload):	
	url = "https://"+host+uri
	headers = {}

	req = requests.post (url, data = payload, headers = headers, auth = HTTPBasicAuth(username, password), verify = False)

	if req.status_code != 200:
		print ("UNKNOWN ERROR: Can't connect to %s failed: %s" % (url, "error"))
		sys.exit (DOWN)

	result = req.json()

	return result

def get_edgeClusterStatus (host, uri, username, password, edgecluster = False, edgenode = False, node = False, critical = False, warning = False):
	ret_code = OK
	msg = "OK"
	perf_data = []

	
	edgeclusterid = get_edgeClusterId(host, username, password, edgecluster)

	response = call_api_get(host,uri+"/"+edgeclusterid+"/status?source=realtime",username,password)

	if (response["edge_cluster_status"]) != "UP":
		ret_code = CRITICAL	
		msg += "CRITICAL - Cluster Status is "+str(response["edge_cluster_status"])

	if ret_code == OK:
		msg = "OK"

	return (ret_code, msg, perf_data)

def get_edgeNodeStatus (host, uri, username, password, edgecluster = False, edgenode = False, node = False, critical = False, warning = False):
	ret_code = OK
	msg = "OK"
	perf_data = []

	
	edgeclusterid = get_edgeClusterId(host, username, password, edgecluster)
	edgenodeid = get_edgeNodeId(host, username, password, edgenode, edgeclusterid)
	response = call_api_get(host,uri+"/"+edgenodeid+"/status?source=realtime",username,password)

	if (response["status"]) != "UP":
		ret_code = CRITICAL	
		msg += "CRITICAL - Cluster Status is "+str(response["status"])

	if ret_code == OK:
		msg = "OK"

	return (ret_code, msg, perf_data)

def get_edgeNodeTunnelStatus (host, uri, username, password, edgecluster = False, edgenode = False, node = False, critical = False, warning = False):
	ret_code = OK
	msg = "OK"
	perf_data = []

	
	edgeclusterid = get_edgeClusterId(host, username, password, edgecluster)
	edgenodeid = get_edgeNodeId(host, username, password, edgenode, edgeclusterid)
	response = call_api_get(host,uri+"/"+edgenodeid+"/status?source=realtime",username,password)

	if (response["tunnel_status"]["status"]) != "UP":
		ret_code = CRITICAL	
		msg += "CRITICAL - Edge Node ["+response["node_display_name"]+"] has Tunnels in status down!"

	if ret_code == OK:
		msg = "OK, "+str(response["tunnel_status"]["up_count"])+" Tunnels up."

	return (ret_code, msg, perf_data)

def get_edgeNodeConnectionStatus (host, uri, username, password, edgecluster = False, edgenode = False, node = False, critical = False, warning = False):
	ret_code = OK
	msg = "OK"
	perf_data = []

	
	edgeclusterid = get_edgeClusterId(host, username, password, edgecluster)
	edgenodeid = get_edgeNodeId(host, username, password, edgenode, edgeclusterid)
	response = call_api_get(host,uri+"/"+edgenodeid+"/status?source=realtime",username,password)

	if (response["status"]) != "UP":
		ret_code = CRITICAL	
		msg += "CRITICAL - Edge Node ["+response["node_display_name"]+"] has connection problems."
	
	if (response["mgmt_connection_status"]) != "UP":
		ret_code = CRITICAL	
		msg += "CRITICAL - Edge Node ["+response["node_display_name"]+"] has no connectivity to the Management Plane."
	
	if (response["control_connection_status"]["status"]) != "UP":
		ret_code = CRITICAL	
		msg += "CRITICAL - Edge Node ["+response["node_display_name"]+"] has no connectivity to the Control Plane."
	
	if (response["node_status"]["mpa_connectivity_status_details"]) != "Client is responding to heartbeats":
		ret_code = CRITICAL	
		msg += "CRITICAL - Edge Node ["+response["node_display_name"]+"] is not responding to heartbeats."

	if ret_code == OK:
		msg = "OK, all Connections up and responding to heartbeats."

	return (ret_code, msg, perf_data)

def get_edgeNodeResourceStatus (host, uri, username, password, edgecluster = False, edgenode = False, node = False, critical = False, warning = False):
	ret_code = OK
	msg = "OK"
	perf_data = []

	
	edgeclusterid = get_edgeClusterId(host, username, password, edgecluster)
	edgenodeid = get_edgeNodeId(host, username, password, edgenode, edgeclusterid)
	response = call_api_get(host,uri+"/"+edgenodeid+"/status?source=realtime",username,password)

	diskusage = round((response["node_status"]["system_status"]["disk_space_used"] / response["node_status"]["system_status"]["disk_space_total"]) * 100,2)
	ramusage = round((response["node_status"]["system_status"]["mem_used"] / response["node_status"]["system_status"]["mem_total"]) * 100,2)
	cpuload = round(response["node_status"]["system_status"]["load_average"][0],2)

	if diskusage >= 80 and diskusage <90:
		if ret_code != CRITICAL:
			ret_code = WARNING	
		msg += "WARNING - Edge Node ["+response["node_display_name"]+"] disk usage: "+diskusage+"%"
	if diskusage >= 90:
		ret_code = CRITICAL	
		msg += "CRITICAL - Edge Node ["+response["node_display_name"]+"] disk usage: "+diskusage+"%"

	if ramusage >= 80 and ramusage <90:
		if ret_code != CRITICAL:
			ret_code = WARNING	
		msg += "WARNING - Edge Node ["+response["node_display_name"]+"] RAM usage: "+diskusage+"%"
	if ramusage >= 90:
		ret_code = CRITICAL	
		msg += "CRITICAL - Edge Node ["+response["node_display_name"]+"] RAM usage: "+diskusage+"%"

	if cpuload >= 80 and cpuload <90:
		if ret_code != CRITICAL:
			ret_code = WARNING	
		msg += "WARNING - Edge Node ["+response["node_display_name"]+"] CPU usage: "+cpuload+"%"
	if cpuload >= 90:
		ret_code = CRITICAL	
		msg += "CRITICAL - Edge Node ["+response["node_display_name"]+"] CPU usage: "+cpuload+"%"

	if ret_code == OK:
		msg = "OK, CPU "+str(cpuload)+"%, RAM "+str(ramusage)+"%, Disk "+str(diskusage)+"%"

	return (ret_code, msg, perf_data)

def get_edgeClusterId(host, username, password, query = False):
	list = call_api_get(host,"/api/v1/edge-clusters",username,password)

	if (query == "*" or query == None or query == False or list["results"][0]["display_name"] == query) and list["result_count"] == 1 :
		return list["results"][0]["id"]
	else:
		results = []
		clusters = []
		for cluster in list["results"]:
			if cluster["display_name"] == query:
				results += cluster["id"]
			clusters += [cluster["display_name"]]

		if len(results) != 0 and len(results) <2:
			return results[0]["id"]
		else:
			print("Cant find Edge Cluster or can't identify it uniquely. Please specify further with parameter -edgecluster. Currently present are "+str(clusters)) 
			exit()

def get_edgeNodeId(host, username, password, query, edgeclusterid = False):
	list = call_api_get(host,"/api/v1/edge-clusters",username,password)

	results = []
	nodes = []

	if edgeclusterid == False or edgeclusterid == None or (list["result_count"] == 1):
		for cluster in list["results"]:
			for member in cluster["members"]:
				if member["display_name"] == query:
					results += [member["transport_node_id"]]
				nodes += [member["display_name"]]
	else: 
		for cluster in list["results"]:
			if cluster["id"] == edgeclusterid:
				for member in cluster["members"]:
					if member["display_name"] == query:
						results += [member["transport_node_id"]]
					nodes += [member["display_name"]]

	if len(results) != 0 and len(results) <2:
		return results[0]
	else:
		print("Cant find given Edge Node or can't identify it uniquely. Please specify further with parameter -edgenode and -edgecluster names. In the current edgecluster, following nodes are present: "+str(nodes))
		exit()


################################################################################
# API Endpoint definition

checks = {
	'edgeclusterstatus' : {
		'api_path' : "/api/v1/edge-clusters",
		'function' : get_edgeClusterStatus
	},
	'edgenodestatus' : {
		'api_path' : "/api/v1/transport-nodes",
		'function' : get_edgeNodeStatus
	},
	'edgenodetunnelstatus' : {
		'api_path' : "/api/v1/transport-nodes",
		'function' : get_edgeNodeTunnelStatus
	},
	'edgenodeconnectionstatus' : {
		'api_path' : "/api/v1/transport-nodes",
		'function' : get_edgeNodeConnectionStatus
	}
	,
	'edgenoderesourcestatus' : {
		'api_path' : "/api/v1/transport-nodes",
		'function' : get_edgeNodeResourceStatus
	}
}

################################################################################
# Import input parameters and execute function that was called

args = p.parse_args ()



(ret_code, msg, perf_data) = checks[args.check]['function'] (str(args.host), checks[args.check]['api_path'], args.username, args.password, args.edgecluster, args.edgenode)

################################################################################
# Output results

if ret_code == 1:
	msg = "WARNING: " + msg
elif ret_code == 2:
	msg = "CRITICAL: " + msg

if perf_data:
	msg += "| " + " ".join (sorted (perf_data))

print (msg.rstrip ())
sys.exit (ret_code)

