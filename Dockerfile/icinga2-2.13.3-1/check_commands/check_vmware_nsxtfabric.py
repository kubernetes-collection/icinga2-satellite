#!/usr/bin/python3
#
SCRIPTNAME= "nsxtfabric2icinga"
# Author	Lukas Rueckerl 
# mailto 	info@lkrkl.dev 
VERSION=	"0.1 / 200423"
#
# Tested with Python 3.11
#
# Aggregates metrics from the NSX-T Manager and Policy API for monitoring - intended for Icinga2
# Be sure to install the "requests"-Pip-Package beforehand


################################################################################
# Import and preset variables 

import argparse
import json
import sys
import decimal
from tokenize import String
import requests
import urllib.parse
from requests.auth import HTTPBasicAuth
from requests.packages.urllib3.exceptions import InsecureRequestWarning # type: ignore
requests.packages.urllib3.disable_warnings(InsecureRequestWarning) # type: ignore

################################################################################
# Define global Variables
OK = 0
WARNING = 1
CRITICAL = 2
DOWN = 3

################################################################################
# Define shell arguments

p = argparse.ArgumentParser (epilog=SCRIPTNAME+' | '+VERSION+' | CC-BY',)

sp = p.add_subparsers (dest = 'check', title = "checks")
nodestatus_p = sp.add_parser ('nodestatus', help = 'Checks all nodes in the cluster for resource and control plane availability')
nodestatussummary_p = sp.add_parser ('nodestatussummary', help = 'Checks all nodes in the cluster for resource and control plane availability')
nodelist_p = sp.add_parser ('nodelist', help = 'Always returns OK, prints a list of all fabric nodes')

# Required Arguments
p.add_argument ('-host', help = "FQDN or IP of your NSX-T Manager VIP")
p.add_argument ('-username', help = "API-User")
p.add_argument ('-password', help = "API-User's Password")
p.add_argument ('-node', help = "The specific resource to query. Provide the displayed name for the Manager Instance from the manager UI.", required=False)

################################################################################
# Check functions

def call_api_get (host,uri, username, password, returnToken = False):
	url = "https://"+host+uri
	req = requests.get(url, auth=HTTPBasicAuth(username, password), verify = False)

	if req.status_code != 200:
		print ("UNKNOWN ERROR: Can't connect to %s failed: %s" % (url, "error"))
		sys.exit (DOWN)

	result = req.json()

	return result

def call_api_post (host,uri, username, password, payload):	
	url = "https://"+host+uri
	headers = {}

	req = requests.post (url, data = payload, headers = headers, auth = HTTPBasicAuth(username, password), verify = False)

	if req.status_code != 200:
		print ("UNKNOWN ERROR: Can't connect to %s failed: %s" % (url, "error"))
		sys.exit (DOWN)

	result = req.json()

	return result

def get_nodeList (host, uri, username, password, node = False, array = False):
	ret_code = OK
	msg = ""
	perf_data = []

	response = call_api_get(host,uri,username,password)

	if (response["result_count"]) == 0:
		ret_code = CRITICAL	
		msg = "No fabric nodes present."		
	else:
		if array != False:
			return response["results"]

		msg = str(response["result_count"])+" Nodes present: "
		for node in response["results"]:
			msg += node["display_name"]+" (ID: "+node["id"]+", Type: "+node["node_deployment_info"]["resource_type"]+"), "
		msg = msg[:-2]

	return (ret_code, msg, perf_data)

def get_nodestatussummary (host, uri, username, password, node = False, critical = False, warning = False):
	ret_code = OK
	msg = ""
	perf_data = []

	node_count = 0

	for nodeobj in get_nodeList(host, uri, username, password, False, True):
		node_count += 1
		returnobject = get_nodestatus(host, uri, username, password, nodeobj["display_name"]) # type: ignore

		if returnobject[0] != OK:
			if ret_code != CRITICAL: 
				ret_code = returnobject[0]
			msg += returnobject[1]+";"

	if ret_code == OK:
		msg = str(node_count)+" nodes, all OK"

	return (ret_code, msg, perf_data)


def get_nodestatus (host, uri, username, password, node = False, critical = False, warning = False):
	ret_code = OK
	msg = ""
	perf_data = []

	nodeArray = get_nodeId(host, username, password, node)
	uri += "/"+str(nodeArray[0])+"/status"
	
	nodeType = nodeArray[1]

	response = call_api_get(host,uri,username,password)

	if (response["status"] != "UP"):
		ret_code = CRITICAL	

	msg = str(nodeType)+" is "+str(response["status"]) +"; "

	if ((response["node_status"]["host_node_deployment_status"] != "NODE_READY" and nodeType == "EdgeNode") or (response["node_status"]["host_node_deployment_status"] != "INSTALL_SUCCESSFUL" and nodeType == "HostNode")):
		ret_code = CRITICAL	
		msg += "Node is not finished deploying; "

	if (response["mgmt_connection_status"] != "UP"):
		ret_code = CRITICAL	
		msg += "Node Management Connection is "+str(response["mgmt_connection_status"])+"; "
		print("CRITICAL: " + msg)
		sys.exit(ret_code)

	if (response["control_connection_status"]["status"] != "UP"):
		if ret_code != CRITICAL: ret_code = WARNING	
		msg += "Node Control Connection is "+str(response["control_connection_status"])+"; "

	if nodeType == "HostNode":
		if (response["agent_status"]["down_count"] > 0):
			ret_code = CRITICAL	
			for agent in response["agent_status"]["agents"]:
				if agent["status"] != "UP":
					msg += "Agent Service "+str(agent["name"])+" is "+str(agent["status"])+"; "
		else:
			msg += "Agent is "+str(response["agent_status"]["status"])+", "+str(response["agent_status"]["up_count"])+" Services up; "

	if (response["tunnel_status"]["status"] != "UP" or response["tunnel_status"]["down_count"] > 0 ):
		ret_code = CRITICAL	
		msg += "Overall Tunnel status is "+str(response["tunnel_status"]["status"])+", Up:"+str(response["tunnel_status"]["up_count"])+" Down:"+str(response["tunnel_status"]["down_count"])+"; "
	else:
		msg += "Tunnels Up:"+str(response["tunnel_status"]["up_count"])+" Down:"+str(response["tunnel_status"]["down_count"])+"; "

	if response["node_status"]["system_status"]["load_average"][0] >= 80 and response["node_status"]["system_status"]["load_average"][0] < 90:
		if ret_code != CRITICAL: ret_code = WARNING		
	if response["node_status"]["system_status"]["load_average"][0] >= 90:
		ret_code = CRITICAL	
	msg += "CPU:"+str(round(response["node_status"]["system_status"]["load_average"][0]))+"%; "
	perf_data.append("'CPU'="+str(round(response["node_status"]["system_status"]["load_average"][0]))+"%;80;90;0;100")

	if nodeType == "EdgeNode":
		if response["node_status"]["system_status"]["edge_mem_usage"]["system_mem_usage"] >= 80 and response["node_status"]["system_status"]["edge_mem_usage"]["system_mem_usage"] < 90:
			if ret_code != CRITICAL: ret_code = WARNING		
		if response["node_status"]["system_status"]["edge_mem_usage"]["system_mem_usage"] >= 90:
			ret_code = CRITICAL	
		msg += "RAM:"+str(round(response["node_status"]["system_status"]["edge_mem_usage"]["system_mem_usage"]))+"%; "

		if response["node_status"]["system_status"]["edge_mem_usage"]["swap_usage"] > 0:
			if ret_code != CRITICAL: ret_code = WARNING
		msg += "SWAP:"+str(round(response["node_status"]["system_status"]["edge_mem_usage"]["swap_usage"]))+"%; "

		for mempool in response["node_status"]["system_status"]["edge_mem_usage"]["datapath_mem_usage_details"]["datapath_mem_pools_usage"]:
			
			if mempool["usage"] >= 80:
				if ret_code != CRITICAL: ret_code = WARNING
				msg += str(mempool["name"])+" Usage:"+str(round(mempool["usage"]))+"% ("+str(mempool["description"])+"); "
			if mempool["usage"] >= 90:
				ret_code = CRITICAL		
				msg += str(mempool["name"])+" Usage:"+str(round(mempool["usage"]))+"% ("+str(mempool["description"])+"); "	

			perf_data.append("'MEMPOOL_"+str(mempool["name"])+"'="+str(round(mempool["usage"]))+"%;80;90;0;100")

		perf_data.append("'RAM'="+str(round(response["node_status"]["system_status"]["edge_mem_usage"]["system_mem_usage"]))+"%;80;90;0;100")
	else:
		memusage = (response["node_status"]["system_status"]["mem_used"]/response["node_status"]["system_status"]["mem_total"])*100
		if memusage >= 80 and memusage < 90:
			if ret_code != CRITICAL: ret_code = WARNING		
		if memusage >= 90:
			ret_code = CRITICAL	
		msg += "RAM:"+str(round(memusage))+"%; "
		perf_data.append("'RAM'="+str(round(memusage))+"%;80;90;0;100")

	if response["node_status"]["system_status"]["disk_space_total"] > 0:
		totaldiskusage = (response["node_status"]["system_status"]["disk_space_used"]/response["node_status"]["system_status"]["disk_space_total"])*100	
		if totaldiskusage >= 80 and totaldiskusage < 90:
			if ret_code != CRITICAL: ret_code = WARNING		
		if totaldiskusage >= 90:
			ret_code = CRITICAL
		perf_data.append("'DISK'="+str(round(totaldiskusage))+"%;80;90;0;100")
		msg += "DISK:"+str(round(totaldiskusage))+"%; "		

	for filesystem in response["node_status"]["system_status"]["file_systems"]:
		diskusage = (filesystem["used"]/filesystem["total"])*100
		if diskusage >= 80:
			if ret_code != CRITICAL: ret_code = WARNING
		if diskusage >= 90:
			ret_code = CRITICAL			

	
	if nodeType == "EdgeNode" and response["node_status"]["maintenance_mode"] != "DISABLED":
		ret_code = DOWN
		msg = "Node is in Maintenance Mode."
	elif nodeType == "HostNode" and response["maintenance_mode"] != "DISABLED":
		ret_code = DOWN
		msg = "Node is in Maintenance Mode."
	else:
		msg = msg[:-2]
		

	return (ret_code, msg, perf_data)

def get_nodeId(host, username, password, query):
	nodes = call_api_get(host,"/api/v1/transport-nodes",username,password)

	results = []

	for node in nodes["results"]:
		if node["display_name"] == query:
			results += [node]	


	if len(results) != 0 and len(results) <2:
		return (results[0]["id"], results[0]["node_deployment_info"]["resource_type"])
	else:
		print("Cant find Node. Please specify.")
		sys.exit (DOWN)



################################################################################
# API Endpoint definition

checks = {
	'nodelist' : {
		'api_path' : "/api/v1/transport-nodes",
		'function' : get_nodeList
	},
	'nodestatus' : {
		'api_path' : "/api/v1/transport-nodes",
		'function' : get_nodestatus
	},
	'nodestatussummary' : {
		'api_path' : "/api/v1/transport-nodes",
		'function' : get_nodestatussummary
	}
}

################################################################################
# Import input parameters and execute function that was called

args = p.parse_args ()

(ret_code, msg, perf_data) = checks[args.check]['function'] (str(args.host), checks[args.check]['api_path'], args.username, args.password, args.node)

################################################################################
# Output results

if ret_code == 1:
	msg = "WARNING: " + msg
elif ret_code == 2:
	msg = "CRITICAL: " + msg

if perf_data:
	msg += "| " + " ".join (sorted (perf_data))

print (msg.rstrip ())
sys.exit (ret_code)

