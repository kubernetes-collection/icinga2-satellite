#!/bin/bash

# Author: Stephan Wild
# Date: 23.09.2022
# Company: EDV-BV GmbH
# Usage:
# -h FQDN
# -p Port
# -t Time in Seconds



while getopts h:p:t: flag
do
    case "${flag}" in
        h) SSLHOST=${OPTARG};;
        p) SSLPORT=${OPTARG};;
        t) SSLEXPIRATION=${OPTARG};;
    esac
done


#SSLHOST=$1
#SSLPORT=$2
#SSLEXPIRATION=$3


if [[ $SSLPORT == "" ]] ; then
	SSLPORT=443
fi


if [[ $SSLEXPIRATION == "" ]] ; then
	SSLEXPIRATION=604800
fi

function check_ssl () {
	echo | \
		openssl s_client -servername $SSLHOST -connect $SSLHOST:$SSLPORT | \
		openssl x509 -enddate -noout -dates -checkend $SSLEXPIRATION

	exitcode=$?

	if [[ $exitcode == 1 ]] ; then
		exit 2
	else
	 	exit 0
	fi
}


function help () {
	echo "Usage:"
	echo "-h Hostname as FQDN"
	echo "-p Hostport"
	echo "-t Time in seconds to check expiration"
}

if [[ $SSLHOST == "" ]] ; then
	help
else
	check_ssl
fi
