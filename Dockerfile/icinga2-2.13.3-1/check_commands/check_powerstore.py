#!/usr/bin/python3
#
# Author	Lukas Rueckerl
# mailto 	lukas.rueckerl@edv-bv.de
#
# Version	1/29012022
#
# Tested with Python 3.10.2
#
# Aggregates metrics from the powerstore API for monitoring - intended for Icinga2
#


################################################################################
# Import and preset variables

import argparse
import json
import sys
import decimal
import requests
from requests.auth import HTTPBasicAuth
from requests.packages.urllib3.exceptions import InsecureRequestWarning
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

# Possible Exit codes
OK = 0
WARNING = 1
CRITICAL = 2
DOWN = 3


################################################################################
# Define shell arguments

p = argparse.ArgumentParser (epilog='powerstore2icinga | Version 1/29012022 | ©edv-bv GmbH',)

sp = p.add_subparsers (dest = 'check', required = True, title = "checks")

# Check for alerts
alerts_p = sp.add_parser ('alerts', help = 'Returns minor and major (not acknowledged and active) alerts | WARNING (only alerts with severity "Minor") | CRITICAL (at least one "Major" alert)')
# Check HardwareHealth
hardwarehealth_p = sp.add_parser ('health', help = 'Returns hardware part state if not healthy (overall Node status, Drives, DIMMs, Fans, IOMOdules, NVRAM) | WARNING (1 part in faulty state) | CRITICAL (2 or more parts in faulty state)')

# CAPACITY METRICS

# Check overall Space Metrics
space_p = sp.add_parser ('space', help = 'Returns physical & logical usage, compaction and provisioning factor all in percent | WARNING (Logical or physical usage >= 80%%, can be set with [-w] parameter) | CRITICAL (Logical or physical usage >= 90%%, can be set with [-c] parameter)')
# Check Overprovisioning
provisioning_p = sp.add_parser ('provisioning', help = 'Returns provisioning factor in percent | CRITICAL (if logical_provisioned exceeds 90%% of max logical capacity (compaction_rate x physical_capacity), can be set with [-c] / [-w] has no effect)')
# Check physical capacity
physical_capacity_p = sp.add_parser ('capacity', help = 'Returns nothing | WARNING (Physical usage >= 80%%, can be set with [-w] parameter) | CRITICAL (Physical usage >= 90%%, can be set with [-c] parameter)')
# Check DDR
ddr = sp.add_parser ('ddr', help = 'Effective logical/physical compaction rate in percent | Exit always 0, warning and critical threshold can be set with [-w] / [-c] parameter )')

# PERFORMANCE METRICS
# Check Latency
latency_p = sp.add_parser ('latency', help = 'Average latency in ms | WARNING (>2ms, can be set with [-w] parameter) | CRITICAL (>5ms, can be set with [-c] parameter)')
# Check avg bandwidth total
bandwidth_p = sp.add_parser ('bandwidth', help = 'Average total throughput in Megabit/s | Exit code always 0')
# Check avg read
read_p = sp.add_parser ('read', help = 'Average read throughput in Megabit/s | Exit code always 0')
# Check avg write
write_p = sp.add_parser ('write', help = 'Average write throughput in Megabit/s | Exit code always 0')
# Check iops
iops_p = sp.add_parser ('iops', help = 'Total IO per second | Exit code always 0')
# Check io size
iosize_p = sp.add_parser ('iosize', help = 'Average IO Size in Kilobyte | Exit code always 0')


# Required Arguments
p.add_argument ('-host', help = "FQDN or IP of the Powerstore system", required=True)
p.add_argument ('-user', help = "API-User", required=True)
p.add_argument ('-passw', help = "API-User's Password", required=True)

p.add_argument ('-w', help = "Threshold for warning state (Integer or float)", type=float, required=False)
p.add_argument ('-c', help = "Threshold for critical state (Integer or float)", type=float, required=False)

################################################################################
# Method definition

def call_api_get (host, uri, apiuser, apipw, returnToken = False):
	url = "https://"+host+uri
	req = requests.get(url, auth=HTTPBasicAuth(apiuser, apipw), verify = False)

	if req.status_code != 200:
		print ("UNKNOWN ERROR: Can't connect to %s failed: %s" % (url, "error"))
		sys.exit (DOWN)

	result = req.json ()

	if returnToken == True:
		result = req.headers['DELL-EMC-TOKEN']

	return result

def call_api_post (host, uri, apiuser, apipw, payload):
	token = call_api_get(host, "/api/rest/appliance" , apiuser, apipw, True)
	url = "https://"+host+uri
	headers = {"DELL-EMC-TOKEN":token, "Content-Type":"application/json"}

	req = requests.post (url, data = payload, headers = headers, auth = HTTPBasicAuth(apiuser, apipw), verify = False)

	if req.status_code != 200:
		print ("UNKNOWN ERROR: Can't connect to %s failed: %s" % (url, "error"))
		sys.exit (DOWN)

	result = req.json ()

	return result

def round_down(value, decimals):
    with decimal.localcontext() as ctx:
        d = decimal.Decimal(value)
        ctx.rounding = decimal.ROUND_DOWN
        return round(d, decimals)

def defectivehardwarecheck(uri, endpoint, apiuser, apipw, critical = False, warning = False):
	ret_code = OK
	msg = "OK"
	perf_data = []

	apiresponse = call_api_get(uri, endpoint, apiuser, apipw)

	if len(apiresponse) == 1:
		ret_code = WARNING
		msg = "A harware component reports an unhealthy status: "
		for item in apiresponse:
			msg = msg+item["name"]+": "+item["lifecycle_state"]+";"

	if len(apiresponse) > 1:
		ret_code = CRITICAL
		msg = "Multiple harware components report an unhealthy status: "
		for item in apiresponse:
			msg = msg+item["name"]+": "+item["lifecycle_state"]+"; "

	return (ret_code, msg, perf_data)

def alertcheck (uri, endpoint, apiuser, apipw, critical = False, warning = False):
	ret_code = OK
	msg = "OK"
	perf_data = []

	apiresponse = call_api_get(uri, endpoint, apiuser, apipw)

	if len(apiresponse) >= 1:
		if apiresponse[0]["severity"] == "Minor":
			ret_code = WARNING
			msg = "There are one or more MINOR alerts active: "
			for item in apiresponse:
				msg = msg+item["severity"]+": "+item["description_l10n"]+" ON SYSTEM "+item["resource_name"]+";"

		if apiresponse[0]["severity"] == "Major":
			ret_code = CRITICAL
			msg = "There are MAJOR alerts present: "
			for item in apiresponse:
				msg = msg+item["description_l10n"]+" ON SYSTEM "+item["resource_name"]+";"

	return (ret_code, msg, perf_data)

def spacecheck(uri, endpoint, apiuser, apipw, critical = False, warning = False):
	ret_code = OK
	msg = ""
	perf_data = []

	payload = '{"entity":"space_metrics_by_cluster","entity_id":"0"}'

	apiresponse = call_api_post(uri, endpoint, apiuser, apipw, payload)
	apiresponse = apiresponse[-1]

	physical_usage =  round_down((apiresponse["physical_used"] / apiresponse["physical_total"])*100,2)
	logical_usage =  round_down((apiresponse["logical_used"] / apiresponse["logical_provisioned"])*100,2)
	ddr =  round_down(apiresponse["data_reduction"]*100,2)

	max_logical =  round_down(apiresponse["data_reduction"]*apiresponse["physical_total"],2)
	provisioned_logical =  round_down(apiresponse["logical_provisioned"],2)
	overprovisioning = round_down((provisioned_logical / max_logical)*100,2)

	if warning != False and (isinstance(warning, int) or isinstance(warning, float)):
		if physical_usage >= warning:
			ret_code = WARNING
	else:
		if physical_usage >= 80:
			ret_code = WARNING

	if critical != False and (isinstance(critical, int) or isinstance(critical, float)):
		if physical_usage >= critical:
			ret_code = CRITICAL
	else:
		if physical_usage >= 90:
			ret_code = CRITICAL


	msg = "PhysicalUsage: "+str(physical_usage)+"%; ProvisionedUsage: "+str(logical_usage)+"%; DataReduction: "+str(ddr)+"%; Provisioned: "+str(overprovisioning)+"%"
	return (ret_code, msg, perf_data)

def overprovisioningcheck(uri, endpoint, apiuser, apipw, critical = False, warning = False):
	ret_code = OK
	msg = ""
	perf_data = []

	payload = '{"entity":"space_metrics_by_cluster","entity_id":"0"}'

	apiresponse = call_api_post(uri, endpoint, apiuser, apipw, payload)
	apiresponse = apiresponse[-1]

	max_logical =  round_down(apiresponse["data_reduction"]*apiresponse["physical_total"],2)
	provisioned_logical =  round_down(apiresponse["logical_provisioned"],2)

	overprovisioning = round_down((provisioned_logical / max_logical)*100,2)

	if critical != False and (isinstance(critical, int) or isinstance(critical, float)):
		if overprovisioning > critical:
			ret_code = CRITICAL
	else:
		if overprovisioning >= 90:
			ret_code = CRITICAL

	msg = "Provisioned "+str(overprovisioning)+"% of current maximum logical capacity"
	return (ret_code, msg, perf_data)

def physical_capacitycheck(uri, endpoint, apiuser, apipw, critical = False, warning = False):
	ret_code = OK
	msg = ""
	perf_data = []

	payload = '{"entity":"space_metrics_by_cluster","entity_id":"0"}'

	apiresponse = call_api_post(uri, endpoint, apiuser, apipw, payload)
	apiresponse = apiresponse[-1]

	physical_usage =  round_down((apiresponse["physical_used"] / apiresponse["physical_total"])*100,2)

	msg="Physical usage: "+str(physical_usage)+"%"

	if warning != False and (isinstance(warning, int) or isinstance(warning, float)):
		if physical_usage >= warning:
			ret_code = WARNING
			msg="Physical usage exceeds "+str(warning)+"%% - currently "+str(physical_usage)+"%"
	else:
		if physical_usage >= 80:
			ret_code = WARNING
			msg="Physical usage exceeds 80%% - currently "+str(physical_usage)+"%"

	if critical != False and (isinstance(critical, int) or isinstance(critical, float)):
		if physical_usage >= critical:
			ret_code = CRITICAL
			msg="Physical usage exceeds "+str(critical)+"%% - currently "+str(physical_usage)+"%"
	else:
		if physical_usage >= 90:
			ret_code = CRITICAL
			msg="Physical usage exceeds 90%% - currently "+str(physical_usage)+"%"

	return (ret_code, msg, perf_data)

def ddrcheck(uri, endpoint, apiuser, apipw, critical = False, warning = False):
	ret_code = OK
	msg = ""
	perf_data = []

	payload = '{"entity":"space_metrics_by_cluster","entity_id":"0"}'

	apiresponse = call_api_post(uri, endpoint, apiuser, apipw, payload)
	apiresponse = apiresponse[-1]

	ddr =  round_down(apiresponse["data_reduction"]*100,2)

	if warning != False and (isinstance(warning, int) or isinstance(warning, float)):
		if ddr <= warning:
			ret_code = WARNING
			msg="Compaction Rate below "+str(warning)+"%% - currently "+ddr+"%"

	if critical != False and (isinstance(critical, int) or isinstance(critical, float)):
		if ddr <= critical:
			ret_code = CRITICAL
			msg="Compaction rate below "+str(critical)+"%% - currently "+ddr+"%"

	msg = "Logical/Physical Compaction Rate: "+str(ddr)+"%"
	return (ret_code, msg, perf_data)

def latencycheck(uri, endpoint, apiuser, apipw, critical = False, warning = False):
	ret_code = OK
	msg = "OK"
	perf_data = []

	payload = '{"entity":"performance_metrics_by_cluster","entity_id":"0"}'

	apiresponse = call_api_post(uri, endpoint, apiuser, apipw, payload)
	apiresponse = apiresponse[-1]

	latency =  round_down(apiresponse["avg_latency"]/1000,2)

	if warning != False and (isinstance(warning, int) or isinstance(warning, float)):
		if latency >= warning:
			ret_code = WARNING
			msg="Physical usage exceeds "+str(warning)+"ms - currently "+str(latency)+"ms"
	else:
		if latency >= 2:
			ret_code = WARNING
			msg="Average latency above 2ms - currently "+str(latency)+"ms"

	if critical != False and (isinstance(critical, int) or isinstance(critical, float)):
		if latency >= critical:
			ret_code = CRITICAL
			msg="Average Latency above "+str(critical)+"ms - currently "+str(latency)+"ms"
	else:
		if latency >= 5:
			ret_code = CRITICAL
			msg="Average Latency above 5ms - currently "+str(latency)+"ms"

	msg = "Average Latency: "+str(latency)+"ms"
	return (ret_code, msg, perf_data)

def bandwidthcheck(uri, endpoint, apiuser, apipw, critical = False, warning = False):
	ret_code = OK
	msg = ""
	perf_data = []

	payload = '{"entity":"performance_metrics_by_cluster","entity_id":"0"}'

	apiresponse = call_api_post(uri, endpoint, apiuser, apipw, payload)
	apiresponse = apiresponse[-1]

	bandwidth =  round_down(apiresponse["total_bandwidth"]/1000000,2)

	msg = "Average bandwidth total: "+str(bandwidth)+" MBit/s"
	return (ret_code, msg, perf_data)

def readcheck(uri, endpoint, apiuser, apipw, critical = False, warning = False):
	ret_code = OK
	msg = ""
	perf_data = []

	payload = '{"entity":"performance_metrics_by_cluster","entity_id":"0"}'

	apiresponse = call_api_post(uri, endpoint, apiuser, apipw, payload)
	apiresponse = apiresponse[-1]

	read_bandwidth =  round_down(apiresponse["read_bandwidth"]/1000000,2)

	msg = "Read operations: "+str(read_bandwidth)+" MBit/s"
	return (ret_code, msg, perf_data)

def writecheck(uri, endpoint, apiuser, apipw, critical = False, warning = False):
	ret_code = OK
	msg = ""
	perf_data = []

	payload = '{"entity":"performance_metrics_by_cluster","entity_id":"0"}'

	apiresponse = call_api_post(uri, endpoint, apiuser, apipw, payload)
	apiresponse = apiresponse[-1]

	write_bandwidth =  round_down(apiresponse["read_bandwidth"]/1000000,2)

	msg = "Write operations: "+str(write_bandwidth)+" MBit/s"
	return (ret_code, msg, perf_data)

def iopscheck(uri, endpoint, apiuser, apipw, critical = False, warning = False):
	ret_code = OK
	msg = ""
	perf_data = []

	payload = '{"entity":"performance_metrics_by_cluster","entity_id":"0"}'

	apiresponse = call_api_post(uri, endpoint, apiuser, apipw, payload)
	apiresponse = apiresponse[-1]

	iops =  round_down(apiresponse["total_iops"],0)

	msg = "Average IO: "+str(iops)+" IO/s"
	return (ret_code, msg, perf_data)

def iosizecheck(uri, endpoint, apiuser, apipw, critical = False, warning = False):
	ret_code = OK
	msg = ""
	perf_data = []

	payload = '{"entity":"performance_metrics_by_cluster","entity_id":"0"}'

	apiresponse = call_api_post(uri, endpoint, apiuser, apipw, payload)
	apiresponse = apiresponse[-1]

	iosize =  round_down(apiresponse["avg_io_size"]/1000,0)

	msg = "Average IO size: "+str(iosize)+" KB"
	return (ret_code, msg, perf_data)

################################################################################
# API Endpoint definition

checks = {
	'alerts' : {
		'api_path' : "/api/rest/alert?select=*&or=(severity.eq.Minor,severity.eq.Major)&state=eq.ACTIVE&order=severity.desc&is_acknowledged=eq.false",
		'function' : alertcheck,
	},
	'health' : {
		'api_path' : '/api/rest/hardware?select=*&lifecycle_state=neq.Healthy&lifecycle_state=neq.Empty',
		'function' : defectivehardwarecheck,
	},
	'space' : {
		'api_path' : '/api/rest/metrics/generate?limit=1&select=*',
		'function' : spacecheck,
	},
	'provisioning' : {
		'api_path' : '/api/rest/metrics/generate?limit=1&select=*',
		'function' : overprovisioningcheck,
	},
	'capacity' : {
		'api_path' : '/api/rest/metrics/generate?limit=1&select=*',
		'function' : physical_capacitycheck,
	},
	'ddr' : {
		'api_path' : '/api/rest/metrics/generate?limit=1&select=*',
		'function' : ddrcheck,
	},
	'latency' : {
		'api_path' : '/api/rest/metrics/generate?limit=1&select=*',
		'function' : latencycheck,
	},
	'bandwidth' : {
		'api_path' : '/api/rest/metrics/generate?limit=1&select=*',
		'function' : bandwidthcheck,
	},
	'read' : {
		'api_path' : '/api/rest/metrics/generate?limit=1&select=*',
		'function' : readcheck,
	},
	'write' : {
		'api_path' : '/api/rest/metrics/generate?limit=1&select=*',
		'function' : writecheck,
	},
	'iops' : {
		'api_path' : '/api/rest/metrics/generate?limit=1&select=*',
		'function' : iopscheck,
	},
	'iosize' : {
		'api_path' : '/api/rest/metrics/generate?limit=1&select=*',
		'function' : iosizecheck,
	}
}

################################################################################
# Import input parameters and execute function that was called

args = p.parse_args ()

(ret_code, msg, perf_data) = checks[args.check]['function'] (args.host, checks[args.check]['api_path'], args.user, args.passw, args.c, args.w)

################################################################################
# Output results

if ret_code == 1:
	msg = "WARNING: " + msg
elif ret_code == 2:
	msg = "CRITICAL: " + msg

if perf_data:
	msg += "| " + " ".join (sorted (perf_data))

print (msg.rstrip ())
sys.exit (ret_code)
