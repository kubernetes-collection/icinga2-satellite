#!/bin/bash


function liveness {
	icinga2 variable list | grep -i "^ZoneName.*$ICINGA2_LOCALZONE"
	retzone=$?
	if [[ $retzone -ne 0 ]] ; then
		/postStart.sh
		icinga2 variable list | grep -i "^ZoneName.*$ICINGA2_LOCALZONE"
        	retzone=$?
	fi
	curl https://localhost:5665 -k > /dev/null 2>&1
	rettcp=$?
	if [[ $retzone -eq 0 ]] && [[ $rettcp -eq 0 ]] ; then
		returnval=0
	else 
		returnval=1
	fi
}

liveness
exit $returnval
