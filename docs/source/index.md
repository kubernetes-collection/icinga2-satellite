% Sphinx Documentation documentation master file, created by
% sphinx-quickstart on Thu Jan  5 08:26:24 2023.
% You can adapt this file completely to your liking, but it should at least
% contain the root `toctree` directive.

# Welcome to icinga2 Satellite's documentation!

```{include} ../../README.md
:relative-images:
  
```

```{toctree}
---
caption: 'Kubernetes Installation:'
maxdepth: 2
---

kubernetes-installation
kubernetes-configuration
```

%```{toctree}
%---
%caption: 'Subtree:'
%maxdepth: 2
%---

%third-chapter
%fourth-chapter
%```

