# Quickstart icinga2 satellite in kubernetes

For more information please read the 
[Documentation](https://kubernetes-collection.gitlab.io/icinga2-satellite)

## 1. About the repository:

In the repository you will find 3 subrepos:

1. Deployments
2. Dockerfile
3. Helm

in the Dockerfile-repository, you will find the dockerfiles, and the needed
shell-scripts to deploy the icinga2 satellite as container. In the Deployment-
repository you will find the kubernetes YAML-Files that are needed to deploy the
satellite in a kubernetes cluster. At least there ist the Helm-repository, where
you will find a chart, to change the variables of the deployment for your usage.

## 2. Dockerfiles usage

documentation in the making


## 3. Deployments usage

documentation in the making


## 4. Helm-Chart usage

If you don't have a Kubernetes Distribution running you can use K3s as your Cluster.
You can install it via 
```
curl -sfL https://get.k3s.io | INSTALL_K3S_EXEC="--disable traefik --write-kubeconfig-mode 644" sh -s -
```


The helm-chart can be modified for your infrastructure with additional variables.

Example:


```
helm install icinga2-satellite . \
  --set deployment.icinga2_master1=$HOSTNAME_MASTER1 \
  --set deployment.icinga2_master1_ip=$FQDN_ODER_IP_MASTER1 \
  --set deployment.icinga2_master2=$HOSTNAME_MASTER2 \
  --set deployment.icinga2_master2_ip=$FQDN_ODER_IP_MASTER2 \
  --set deployment.icinga2_parentzone=$UEBERGEORDNETE_ZONE \
  --set deployment.icinga2_localzone=$LOKALE_ZONE \
  --set deployment.icinga2_ticket=$ZERT_TICKET_VON_MASTER \
  --set deployment.hostname=$HOSTNAME_SATELLIT_ENDPOINT
  ```

## 5. Create ingress for communication with correct TCP port

It is necessary to deploy a ingress controller, for communication of the agents
with the satellite. Any ingress controller can open a TCP port (80, 443), but if
you don't want to change the communication ports on all agents, you need to
install the nginx ingress.

```
kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-v1.5.1/deploy/static/provider/cloud/deploy.yaml 
```

After the Installation there are a few changes in the configuration and you need
a configmap with the ports the ingress should open

First of all you have to create a configmap:

```
echo '
apiVersion: v1
kind: ConfigMap
metadata:
  name: tcp-services
  namespace: ingress-nginx
data:
  5665: "icinga2/icinga2-satellite-service:5665"
' > tcp-services.yaml

kubectl apply -f tcp-services.yaml

```
After deploying the configmap you have to tell the ingress controller to use
the map


```
kubectl edit deployment -n ingress-nginx ingress-nginx-controller

### Add to Container Args the following line
        - --tcp-services-configmap=$(POD_NAMESPACE)/tcp-services
```
When the deployment was restarted, you have to add the configured TCP Port to
the nginx service.

```
kubectl edit svc -n ingress-nginx ingress-nginx-controller

### ADD TO PORTS SECTION

- name: proxied-icinga2-5665
  port: 5665                
  protocol: TCP             
  targetPort: 5665
  ```
